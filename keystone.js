// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');
var swig = require('swig');

// Disable swig's bulit-in template caching, express handles it
swig.setDefaults({ cache: false });

keystone.init({
    'name': 'Standing Voice',
    'brand': 'Standing Voice',
    'sass': 'public',
    'static': 'public',
    'favicon': 'public/favicon.ico',
    'views': 'templates/views',
    'view engine': 'swig',
    'custom engine': swig.renderFile,
    'auto update': true,
    'session': true,
    'auth': true,
    'trust proxy': true,
    'user model': 'User',
    'cookie secret': '0b$5B,nmmk|Wfp--rA~3E[}X:wof0c(A@a<eTw|B:Z=9nTQ-98k#P[<mhESf.XIf',
    'wysiwyg cloudinary images': true
});

keystone.import('models');

keystone.set('locals', {
    _: require('underscore'),
    env: keystone.get('env'),
    utils: keystone.utils,
    editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'homepage': [
		'HomepageBanner',
		'OfficeAddress'
	],
	'map': [
		'Marker',
		'MapStyle'
	],
	'about us': [
		'Team',
		'TeamMember'
	],
	'homepage and about us text snippets': [
		'TextSnippet'
	],
	'programmes': [
		'Programme',
		'CaseStudy'
	],
	'standingVoices': [
		'StandingVoice',
		'StandingVoicesContent'
	],
	'albinism': [
		'AlbinismPageSection'
	],
	'resources': [
		'File',
		'Image',
		'VimeoVideo'
	],
	'get involved' : [
		'Event',
		'Fundraiser',
		'FundraisingCategory',
		'FundraisingCallToAction'
	],
	'programmes and fundraising carousels': [
		'Carousel'
	],
	'donate': [
		'DonationCause',
		'DonationsContent'
	],
    'administrators': 'User'
});

keystone.start();
