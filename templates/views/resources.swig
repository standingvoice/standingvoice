{% extends "../layouts/default.swig" %}

{%- block masterheaderclass %} header__parallax{% endblock -%}

{%- block headerclass %} headertype__parallax{% endblock -%}

{% block headercopy %}

   <div class="row">
      <div class="large-12 columns">


         <div class="title title__wrapper" id="jsTitleHook">
            <div class="title__container">

               <div class="title__container--main ui image transition hidden">
                  <h1>Resources</h1>
               </div>
               {# <div class="title__container--sub ui image transition hidden">
                  <h2>Here you will find a whole host of resources that will provide ideas and guidance on our work</h2>
               </div> #}

            </div>
         </div>



      </div>
   </div>

   {% include "../partials/headerparallax.swig" with {url:'../img/parallax_header-resources.jpg'} only %}


{% endblock %}

{% block content %}

      <section class="section section__lightgreen" id="jsScrollToHook">
         <div class="row resource resource__playing">

            <div class="large-8 medium-8 small-12 columns">

               <div class="resource__playing--share">

                  <ul class="share share__icons">
                    <li class="share__icons--twitter"><a href="#_" id="twbtn" class="s_twitter"><i class="fa fa-twitter"></i></a></li>
                    <li class="share__icons--facebook"><a href="#_" id="fbbtn"><i class="fa fa-facebook-official"></i></a></li>
                    <li class="share__icons--google"><a id="plusbtn" class="ps_btn share s_plus g-interactivepost"
                       data-contenturl="https://vimeo.com/{{ data.featured.foreignId }}"
                       data-contentdeeplinkid=""
                       data-clientid="801146658673-74hpbts1l7265ma0jt5unthp2p7ug26m.apps.googleusercontent.com"
                       data-cookiepolicy="single_host_origin"
                       data-prefilltext=""
                       data-calltoactionlabel="CREATE"
                       data-calltoactionurl="https://vimeo.com/{{ data.featured.foreignId }}"
                       data-calltoactiondeeplinkid="https://vimeo.com/{{ data.featured.foreignId }}">
                       <i class="fa fa-google-plus"></i></a></li>
                  </ul>

               </div>

            <div class="resource__playing--video">
               <div class="flex-video vimeo widescreen">
                  <iframe id="player--iframe" src="https://player.vimeo.com/video/{{ data.featured.foreignId }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
               </div>
            </div>

         </div>

         <div class="large-4 medium-4 small-12 end columns">

            <div class="resource__playing--desc">

               <p id="player--desc">{{ data.featured.description }}</p>

            </div>

         </div>

      </div>

      <div class="whatson">

         <div class="row">

            <div class="large-12 columns">
               <div class="whatson__container">

                  <i class="fa fa-play-circle-o"></i>
                  <p><span id="player--title">{{ data.featured.title }}</span><br><span>Now Playing</span></p>

               </div>
            </div>

         </div>

      </div>

   </section>

   <section class="section section__white">

      {% for video in data.videos %}
         {% if loop.first %}
            <div class="row resource videogrid">
         {% elseif loop.index0 % 4 == 0 %}
            <div class="row">
         {% endif %}

         {% include "../partials/video.swig" with video only %}

         {% if loop.index % 4 == 0 or loop.last %}
            </div>
         {% endif %}
      {% endfor %}

      <div class="row viddownload">

         <div class="large-offset-2 large-4 medium-offset-1 medium-5 small-12 columns">

            <p>All of our videos are free to download and share.</p>

         </div>
         <div class="large-4 medium-4 end columns">

            <div class="sv_button blue"><a href="https://vimeo.com/user5961579/videos">download <i class="fa fa-download"></i></a></div>

         </div>

      </div>


   </section>



   <section class="section section__white">

      <div class="paddingmob__remove">

      <div class="row">
         <div class="large-12 columns resourcetitle">
            <span>
            images
            </span>
            <p>All images are free to download and share
               {% if data.files['image asset pack'] %}Download the asset pack below.{% endif %}
            </p>
            {% if data.files['image asset pack'] %}
               <div class="sv_button blue"><a href="{{ data.files['image asset pack'][0].file.url }}">download <i class="fa fa-download"></i></a></div>
            {% endif %}
         </div>
      </div>

         {% for image in data.images %}

         {% if loop.index0 % 3 == 0 %} <div class="row">{% endif %}

            <div class="large-4 medium-4 columns">

               {% if image.small %}
                  <div class="assetimage__container"><img src="{{ image.small.url }}" alt=""></div>
               {% elseif image.medium %}
                  <div class="assetimage__container"><img src="{{ image.medium.url }}" alt=""></div>
               {% elseif image.full %}
                  <div class="assetimage__container"><img src="{{ image.full.url }}" alt=""></div>
               {% endif %}

               <div class="viddesc viddesc__container">

                  <ul>
                     {% if image.small %}
                        <li><a href="{{ image.small.url }}">{{ image.smallDimensions }}</a></li>
                        {% if image.medium or image.full %}
                           <li class="divider">|</li>
                        {% endif %}
                     {% endif %}
                     {% if image.medium %}
                        <li><a href="{{ image.medium.url }}">{{ image.mediumDimensions }}</a></li>
                        {% if image.full %}
                           <li class="divider">|</li>
                        {% endif %}
                     {% endif %}
                     {% if image.full %}
                        <li><a href="{{ image.full.url }}">{{ image.fullDimensions }}</a></li>
                     {% endif %}
                  </ul>

               </div>

            </div>

            {% if loop.index % 3 == 0 or loop.last %}
               </div>
            {% endif %}

         {% endfor %}

      </div>

   </section>


   <section class="section section__white">

      <div class="row">
         <div class="large-12 columns resourcetitle">
            <span>
            Fundraising Materials
            </span>
            <p>
               {% if data.files['fundraising materials pack'] %}Download the asset pack below.{% endif %}
            </p>
            {% if data.files['fundraising materials pack'] %}
               <div class="sv_button blue"><a href="{{ data.files['fundraising materials pack'][0].file.url }}">download all <i class="fa fa-download"></i></a></div>
            {% endif %}
         </div>
      </div>

      <div class="row" data-equalizer>

         {% include "../partials/files.swig" with { files: data.files['fundraising material'] } only %}

      </div>
   </section>

   <section class="section section__white">

      <div class="row">
         <div class="large-12 columns resourcetitle">
            <span>
            Reports
            </span>
            <p>
               {% if data.files['reports pack'] %}Download the asset pack below.{% endif %}
            </p>
            {% if data.files['reports pack'] %}
               <div class="sv_button blue"><a href="{{ data.files['reports pack'][0].file.url }}">download all <i class="fa fa-download"></i></a></div>
            {% endif %}
         </div>
      </div>

      <div class="row" data-equalizer>

         {% include "../partials/files.swig" with { files: data.files.report } only %}

      </div>

   </section>

   <section class="section section__white">

      <div class="row">
         <div class="large-12 columns resourcetitle">
            <span>
            Published Articles
            </span>
            <p>
               {% if data.files['article pack'] %}Download the asset pack below.{% endif %}
            </p>
            {% if data.files['article pack'] %}
               <div class="sv_button blue"><a href="{{ data.files['article pack'][0].file.url }}">download all <i class="fa fa-download"></i></a></div>
            {% endif %}
         </div>
      </div>

      <div class="row" data-equalizer>

         {% include "../partials/files.swig" with { files: data.files['published article'] } only %}

      </div>

   </section>

   <section class="section section__white">

      <div class="row">
         <div class="large-12 columns resourcetitle">
            <span>
            Press Releases
            </span>
            <p>
               {% if data.files['press release pack'] %}Download the asset pack below.{% endif %}
            </p>
            {% if data.files['press release pack'] %}
               <div class="sv_button blue"><a href="{{ data.files['press release pack'][0].file.url }}">download all <i class="fa fa-download"></i></a></div>
            {% endif %}
         </div>
      </div>

      <div class="row" data-equalizer>

         {% include "../partials/files.swig" with { files: data.files['press release'] } only %}

      </div>

   </section>

{% endblock %}

{% block js %}
   <script src="https://apis.google.com/js/platform.js" async defer></script>
   <script src="../js/vendor/SocialShare.min.js"></script>
    <script>
      $(document).ready(function(){
         UI.binds.vimeo();
      });

      (function(window, document, undefined){

         'use strict';

         document.addEventListener('DOMContentLoaded', function(){

            $('#fbbtn').on('click', function(){

               var _url = $('#player--iframe').attr('src'),
                  _desc = $('#player--desc').text();

               FB.ui({
                 method: 'feed',
                 link: _url,
                 caption: _desc,
               }, function(response){
                  console.log(response);
               });

            });


            //-------- end share buttons


             //these are optional
             UI.binds.headerParallaxSection();
             UI.binds.shareButtonUrls();

         });

     })(window, document);
    </script>
{% endblock %}
