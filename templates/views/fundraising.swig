{% extends "../layouts/default.swig" %}

{%- block masterheaderclass %} header__parallax{% endblock -%}

{%- block headerclass %} headertype__parallax{% endblock -%}

{% block headercopy %}

   <div class="row">
      <div class="large-12 columns">


         <div class="title title__wrapper" id="jsTitleHook">
            <div class="title__container">

               <div class="title__container--main ui image transition hidden">
                  <h1>Get involved</h1>
               </div>

            </div>
         </div>



      </div>
   </div>

   {% include "../partials/headerparallax.swig" with {url:'../img/parallax_header-fundraising.jpg'} only %}


{% endblock %}


{% block content %}

{% if data.shouldShowCtaSection == true %}

<section class="section section__lightgreen fundheader">

   <div class="fundheader__wrapper">

      <div class="row">

         <div class="large-5 medium-5 small-12 columns">

			<span>&ldquo;&rdquo;</span>
			<h2 class="fundheader__wrapper--quote">{{data.ctaQuote|safe}}</h2>

         </div>

		 <div class="large-6 medium-7 small-12 large-offset-1 columns">
			{{data.ctaText|safe}}
			<p><a href="mailto:info@standingvoice.org" class="sv_button blue">{{data.buttonText|safe}}</a></p>
		 </div>

      </div>

   </div>
	
</section>
{% endif %}


<section class="section section__white">

   {% include "../partials/headercarousel.swig" with data.carousel only%}

</section>


{% for category in data.fundraisingCategories %}
    <section class="section section__white">
        <!---fundraising section type-->
        <div class="fundtype fundtype__wrapper">
            <div class="row">
                <div class="large-12 columns">
                <div class="fundtype__wrapper--title {% if loop.first %} first{% endif %}">
                        <h2>{{ category.title }}</h2>
                        <p>{{ category.description }}</p>
                    </div>
                </div>
            </div>
            {% for fundraiser in category.fundraisers %}
                {% if loop.index0 % 2 == 0 %}
                    <div class="row ltr">
                        <div class="large-3 columns">
                            <div class="fundtype__wrapper--name">
                                <p>{{ fundraiser.name }}</p>
                                <p>{{ fundraiser.occupation }}</p>
                            </div>
                        </div>
                        <div class="large-8 large-offset-1 columns">
                            <div class="fundtype__wrapper--quote">
                                {{ fundraiser.quote.html | raw }}
                            </div>
                        </div>
                    </div>
                {% else %}
                    <div class="row rtl">
                        <div class="large-8 columns">
                            <div class="fundtype__wrapper--quote">
                                {{ fundraiser.quote.html | raw }}
                            </div>
                        </div>
                        <div class="large-3 large-offset-1 columns">
                            <div class="fundtype__wrapper--name">
                                <p>{{ fundraiser.name }}</p>
                                <p>{{ fundraiser.occupation }}</p>
                            </div>
                        </div>
                    </div>
                {% endif %}
            {% endfor %}
        </div>
    </section>
{% endfor %}

<section class="section section__green">

   <div class="events events__wrapper">

      <div class="row">

         <div class="large-7 columns end">

            <h2>Events</h2>

            <p>There are loads of ways to get involved with Standing Voice. Find the event or fundraising activity for you here.</p>

         </div>

      </div>

      <div class="row">

         <div class="large-2 medium-2 columns">

            <ul class="datelist hide-for-small">
                {% for event in data.events %}
                    <li>
                        <div class="datelist__container">
                            <span class="datelist__container--month">{{ event._.date.format('MMM') }}</span>
                            <span class="datelist__container--day">{{ event._.date.format('D') }}</span>
                        </div>
                    </li>
                {% endfor %}
            </ul>

         </div>

         <div class="large-10 medium-9 columns">
            <ul class="datecontent">
                {% for event in data.events %}
                    <li>
                        <div class="datecontent__container" data-equalizer>

                               <ul class="datelist show-for-small-only">
                                   <li>
                                       <div class="datelist__container">
                                           <span class="datelist__container--month">{{ event._.date.format('MMM') }}</span>
                                           <span class="datelist__container--day">{{ event._.date.format('D') }}</span>
                                       </div>
                                   </li>
                               </ul>

                               <div class="datecontent__container--mobile">
                                 <p class="datecontent__container--title">{{ event.title }}</p>
                                 <p class="datecontent__container--desc">{{ event.description.html | raw }}</p>
                               </div>
                        </div>
                    </li>
                {% endfor %}
            </ul>
         </div>
      </div>
   </div>
</section>



<section class="section section__grey fundplan">

   <div class="row">

      <div class="large-6 medium-5 columns">

         <img src="/img/icons/research_icon.png" alt="">

      </div>
      <div class="large-6 medium-7 columns">
         <h4>Planning a <br>fundraising activity</h4>
         <p>Visit our resources page to get all the information you need</p>
         <a class="sv_button product" href="/resources">Go To Resources</a>
      </div>

   </div>

</section>



{% endblock %}

{% block js %}
<script>

   (function(window, document, undefined){

      'use strict';

      document.addEventListener('DOMContentLoaded', function(){

          //these are optional
          UI.binds.headerCarousel();
          UI.binds.headerParallaxSection();

      });

  })(window, document);

</script>
{% endblock %}
