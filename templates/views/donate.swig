{% extends "../layouts/default.swig" %}

{%- block masterheaderclass %} header__donate{% endblock -%}

{%- block headerclass %} headertype__purchase{% endblock -%}

{% block headercopy %}{% endblock %}


{% block content %}

<section class="section section__red">

   <div class="row">
      <div class="large-5 medium-6 columns">

         <form class="stripeform">

              <h1>Your Donation</h1>


              <div class="row collapse">
                  <div class="large-6 medium-6 small-6 columns">
                      <span class="prefix">Amount</span>
                      <span class="divider"></span>
                  </div>
                  <div class="large-6 medium-6 small-6 columns left">
                      <span class="currency">&pound;</span>
                      <input id="stripe--opt-amount" class="sv_input" type="number" min="1"
                      {% if data.donationCause %}
                        value="{{ data.donationCause.price }}" disabled >
                      {% elseif data.donationAmount %}
                        value="{{ data.donationAmount }}" >
                      {% else %}
                        value="15" >
                      {% endif %}
                  </div>
              </div>
              <div class="row">
                  <div class="large-12 columns">
                     <div class="row">
                        <div class="large-8 medium-8 large-offset-2 medium-offset-2 columns">
                           <input id="stripe--opt-giftaid" type="checkbox"><label for="stripe--opt-giftaid" data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip"
                           title="Tick the first box to add Gift Aid to your donation. This will increase the value of your donation at no extra cost to you. If you pay tax in the UK, Standing Voice can reclaim the basic rate tax (25%) on your gift. If you donate £100, it is worth £125 to Standing Voice.
Tick the second box to make your gift a monthly recurring donation. Monthly payments are vital for ensuring the long-term survival of Standing Voice’s life-saving services.">Gift Aid <i class="fa fa-question-circle"></i></label>
                        </div>
                     </div>

                     <div class="row">
                        <div class="large-8 medium-8 large-offset-2 medium-offset-2 columns">
                          <input id="stripe--opt-recurring" type="checkbox"><label for="stripe--opt-recurring">Make this a recurring <br class="hide-for-large-up">monthly <br class="show-for-large-only">donation</label>
                        </div>
                     </div>
                  </div>
              </div>
              <div class="row">
                  <a href="/donations/donate" id="stripe--trigger" class="sv_button white big">PAY NOW <i class="fa fa-credit-card"></i></a>
				  <p class="recaptcha-text" >This site is protected by reCAPTCHA and the Google
					  <a class="recaptcha-text" href="https://policies.google.com/privacy">Privacy Policy</a> and
					  <a class="recaptcha-text" href="https://policies.google.com/terms">Terms of Service</a> apply.
				  </p>
				  <div class="donation-error hide">There was an error processing your donation.<p>Please refresh and try again.</p></div>
              </div>
         </form>


      </div>

      <div class="large-6 medium-6 large-offset-1 columns">

         <div class="donatecontext">

            <div class="donatecontext__wrapper" id="jsContextHook">
                {% if data.donationCause %}
                    {# this is the content if you are donating to a specific cause #}
                    <div class="donatecontext__wrapper--image">
                        <img src="{{ data.donationCause._.image.fill(700, 540, { gravity: 'north' }) }}" alt="">
                    </div>
                    <h3 class="donatecontext__wrapper--header">{{ data.donationCause.title }}</h3>
                    <p class="donatecontext__wrapper--copy">{{ data.donationCause.description.complete }}</p>
                {% else %}
                    {# this is the content for the generic donate page #}
                    <h3 class="donatecontext__wrapper--header">Donate to Standing Voice</h3>
                    <p class="donatecontext__wrapper--copy">100% of your donations will fund our life-saving programmes </p>
                {% endif %}
            </div>

         </div>

      </div>
   </div>

</section>


<section class="section section__grey donationideas">

   <div class="row">

      <div class="large-5 medium-5 columns">

         <h4>Donation ideas of <br>your own?</h4>
         <img src="/img/donate_other-icon.png" alt="">

      </div>
      <div class="large-6 medium-6 large-offset-1 medium-offset-1 columns">
         <p class="donationideas--copy">If you have your own ideas about how to support our work, please get in touch with one of our team.</p>
         <a class="sv_button product" href="mailto:harry@standingvoice.org">Contact us</a>
      </div>

   </div>

</section>

{% endblock %}

{% block js %}
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script>
        var handler = StripeCheckout.configure({
            key: '{{ process.env.STRIPE_PUBLIC }}',
            token: function(token) {
                var data = {
                    token: token,
                    amount: Number($('#stripe--opt-amount').val()) * 100
                };

                data.monthly = $('#stripe--opt-recurring').is(':checked');

				grecaptcha.ready(function() {
					grecaptcha.execute('{{ process.env.RECAPTCHA_SITE_KEY }}', {action: 'donate'})
							.then(function(recaptchaToken) {
						data.recaptchaToken = recaptchaToken;
						
						$.post(
								'/api/donate',
								data,
								function(data, status, jqXHR) {
									if (data.recaptchaError === true) {
										$('.donation-error').removeClass('hide');
									}
								}
						);
					});
				});
            }
        });

        $('#stripe--opt-amount').on('keydown', function(e) {
            // todo - validate numbers
        });

        $('#stripe--trigger').on('click', function(e) {

            var amount = Number($('#stripe--opt-amount').val()) * 100;
            var description = $('#stripe--opt-recurring').is(':checked') ? 'Monthly donation' : 'Single donation';
            var options = {
                name: 'Standing Voice',
                description: description,
                currency: 'gbp',
                amount: amount,
                image: "/img/sv_logo-mark.png"
            }

            if ($('#stripe--opt-giftaid').is(':checked')) {
                options.address = true;
            } else {
                options.address = false;
            }

            handler.open(options);
            e.preventDefault();
        });

        // Close Checkout on page navigation
        $(window).on('popstate', function() {
            handler.close();
        });
</script>
{% endblock %}
