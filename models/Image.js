var keystone = require('keystone');
var Types = keystone.Field.Types;
var Image = new keystone.List('Image', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' }
});

var storage = new keystone.Storage({
	adapter: require('keystone-storage-adapter-s3'),
	s3: {
		key: process.env.S3_KEY, // required; defaults to process.env.S3_KEY
		secret: process.env.S3_SECRET, // required; defaults to process.env.S3_SECRET
		bucket: process.env.S3_BUCKET // required; defaults to process.env.S3_BUCKET
	},
	schema: {
		bucket: true, // optional; store the bucket the file was uploaded to in your db
		etag: true, // optional; store the etag for the resource
		path: true, // optional; store the path of the file in your db
		url: true, // optional; generate & store a public URL
	},
});

Image.add({
	title: { type: String, required: true, index: true, initial: true },
	full: { type: Types.File, storage: storage },
	fullDimensions: { type: String },
	medium: { type: Types.File, storage: storage },
	mediumDimensions: { type: String },
	small: { type: Types.File, storage: storage },
	smallDimensions: { type: String }
});

Image.register();
