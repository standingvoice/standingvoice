var keystone = require('keystone');
var Types = keystone.Field.Types;
var MapStyle = new keystone.List('MapStyle', {
	map: { name: 'key' },
	nocreate: true,
	nodelete: true
});

MapStyle.add({
	key: { type: String, required: true, index: true },
	content: { type: Types.Textarea }
});

MapStyle.register();
