var keystone = require('keystone');
var Types = keystone.Field.Types;
var StandingVoicesContent = new keystone.List('StandingVoicesContent', {
	autokey: { path: 'key', from: 'name', unique: true }
});

StandingVoicesContent.add({
	name: { type: String, required: true, initial: true },
	topSection: { type: String, required: true, initial: true },
	bottomSection: { type: String, required: true, initial: true },
	middleImage: { type: Types.CloudinaryImage, autoCleanup: true, initial: true }
});

StandingVoicesContent.register();
