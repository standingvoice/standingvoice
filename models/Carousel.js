var keystone = require('keystone');
var Types = keystone.Field.Types;
var Carousel = new keystone.List('Carousel', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' },
    nocreate: true,
    nodelete: true
});

Carousel.add({
    title: { type: String, required: true, index: true, initial: true },
    images: { type: Types.CloudinaryImages, autoCleanup: true, folder: 'carousels' }
});

Carousel.register();
