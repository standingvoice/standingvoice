var keystone = require('keystone');
var Types = keystone.Field.Types;
var FundraisingCategory = new keystone.List('FundraisingCategory', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' },
    sortable: true
});

FundraisingCategory.add({
    title: { type: String, required: true, index: true, initial: true },
    description: { type: Types.Textarea }
});

FundraisingCategory.relationship({
    path: 'fundraisers', ref: 'Fundraiser', refPath: 'category'
});

FundraisingCategory.register();
