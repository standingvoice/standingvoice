var keystone = require('keystone');
var Types = keystone.Field.Types;
var Team = new keystone.List('Team', {
    sortable: true,
    defaultSort: 'sortOrder',
    autokey: { path: 'key', from: 'name', unique: true }
});

Team.add({
    name: { type: String, initial: true, required: true, index: true },
});

Team.relationship({
    path: 'members', ref: 'TeamMember', refPath: 'team'
});

Team.register();
