var keystone = require('keystone');
var Types = keystone.Field.Types;
var TextSnippet = new keystone.List('TextSnippet', {
    map: { name: 'key' },
    nocreate: true,
    nodelete: true
});

TextSnippet.add({
    key: { type: String, required: true, index: true, initial: true, noedit: true },
    content: { type: Types.Textarea }
});

TextSnippet.register();
