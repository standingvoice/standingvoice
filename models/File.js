var keystone = require('keystone');
var Types = keystone.Field.Types;
var File = new keystone.List('File', {
    autokey: { path: 'key', from: 'usage', unique: true },
    map: { name: 'usage' }
});

var storage = new keystone.Storage({
	adapter: require('keystone-storage-adapter-s3'),
	s3: {
		key: process.env.S3_KEY,
		secret: process.env.S3_SECRET,
		bucket: process.env.S3_BUCKET,
		generateFilename: function(file, param, cb) {
			var originalFileName = file.originalname;
			var extension = originalFileName.slice(originalFileName.indexOf('.'));
			var fileNameWithExtension = file.filename + extension;
			
			cb(null, fileNameWithExtension);
		}
	},
	schema: {
		bucket: true, // optional; store the bucket the file was uploaded to in your db
		etag: true, // optional; store the etag for the resource
		path: true, // optional; store the path of the file in your db
		url: true, // optional; generate & store a public URL
	},
});

File.add({
    usage: { type: Types.Select, required: true, index: true, initial: true, options: 'fundraising material, report, published article, press release, image asset pack, fundraising materials pack, reports pack, article pack, press release pack', default: 'fundraising material' },
	file: { type: Types.File, storage: storage },
    summary: { type: Types.Textarea },
    created: { type: Types.Date, default: Date.now }
});

File.register();
