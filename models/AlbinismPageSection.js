var keystone = require('keystone');
var Types = keystone.Field.Types;
var AlbinismPageSection = new keystone.List('AlbinismPageSection', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' },
    sortable: true,
    defaultSort: 'sortOrder'
});

AlbinismPageSection.add({
    title: { type: String, required: true, index: true, initial: true },
    content: { type: Types.Markdown, height: 360 }
});

AlbinismPageSection.register();
