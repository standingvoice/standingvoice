var keystone = require('keystone');
var Types = keystone.Field.Types;
var Event = new keystone.List('Event', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' }
});

Event.add({
    title: { type: String, initial: true, required: true },
    description: { type: Types.Markdown },
    date: { type: Types.Date }
});

Event.register();
