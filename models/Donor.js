var keystone = require('keystone');
var Types = keystone.Field.Types;
var Donor = new keystone.List('Donor', { hidden: true });

Donor.add({
    email: { type: Types.Email, initial: true, required: true, index: true },
    stripeId: { type: String, initial: true, required: true }
});

Donor.register();
