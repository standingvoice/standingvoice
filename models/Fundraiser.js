var keystone = require('keystone');
var Types = keystone.Field.Types;
var Fundraiser = new keystone.List('Fundraiser', {
    autokey: { path: 'key', from: 'name', unique: true },
    sortable: true
});

Fundraiser.add({
    name: { type: String, required: true, initial: true },
    occupation: { type: String, required: true, initial: true },
    quote: { type: Types.Markdown },
    category: { type: Types.Relationship, ref: 'FundraisingCategory' }
});

Fundraiser.register();
