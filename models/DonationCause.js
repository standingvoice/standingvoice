var keystone = require('keystone');
var Types = keystone.Field.Types;
var DonationCause = new keystone.List('DonationCause', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' },
    sortable: true,
    defaultSort: 'sortOrder'
});

DonationCause.add({
    title: { type: String, required: true, index: true, initial: true },
    price: { type: Types.Money, format: '£0,0.00', required: true, initial: true },
    image: { type: Types.CloudinaryImage, autoCleanup: true },
    description: {
        summary: { type: Types.Textarea, height: 80 },
        complete: { type: Types.Textarea, height: 360 }
    }
});

DonationCause.register();
