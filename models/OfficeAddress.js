var keystone = require('keystone');
var OfficeAddress = new keystone.List('OfficeAddress', {
	autokey: { path: 'key', from: 'name', unique: true }
});

OfficeAddress.add({
	name: { type: String, initial: true, required: true, index: true},
	addressLine1: { type: String, initial: true },
	addressLine2: { type: String, initial: true },
	city: { type: String, initial: true },
	postCode: { type: String, initial: true }
});

OfficeAddress.register();
