var keystone = require('keystone');
var Types = keystone.Field.Types;
var donationsContent = new keystone.List('DonationsContent', {
	autokey: { path: 'key', from: 'name', unique: true },
	map: { name: 'name' }
});

donationsContent.add({
	name: { type: Types.Text, initial: true, required: true, index: true, noedit: true },
	topSectionHeader: { type: Types.Text, initial: true, required: true },
	topSectionContent: { type: Types.Textarea, initial: true, required: true },
	topSectionCTA: { type: Types.Text, initial: true, required: true },
	topSectionButtonText: { type: Types.Text, initial: true, required: true },
	defaultDonationAmount: { type: Types.Number, initial: true, required: true }
});

donationsContent.register();
