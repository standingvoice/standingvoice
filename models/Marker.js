var keystone = require('keystone');
var Types = keystone.Field.Types;
var Marker = new keystone.List('Marker', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' }
});

Marker.add({
    title: { type: String, required: true, index: true, initial: true },
    lat: { type: String },
    lng: { type: String },
    category: { type: Types.Select, options: 'Health, Education, Advocacy, Community, Kilisun' }
});

Marker.register();
