var keystone = require('keystone');
var Types = keystone.Field.Types;
var CaseStudy = new keystone.List('CaseStudy', {
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' }
});

CaseStudy.add({
    featured: { type: Boolean },
    image: { type: Types.CloudinaryImage, autoCleanup: true, folder: 'casestudy-teasers' },
    title: { type: String, required: true, index: true, initial: true },
    teaser: { type: Types.Textarea, height: 80 },
    url: { type: Types.Url },
    programme: { type: Types.Relationship, ref: 'Programme' },
});

CaseStudy.defaultColumns = "title, featured";

CaseStudy.register();
