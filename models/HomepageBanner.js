var keystone = require('keystone');
var Types = keystone.Field.Types;
var HomepageBanner = new keystone.List('HomepageBanner', {
	map: { name: 'key' }
});

HomepageBanner.add({
	key: { type: String, required: true, index: true },
	content: { type: Types.Html, wysiwyg: true},
	enabled: { type: Boolean }
});

HomepageBanner.register();
