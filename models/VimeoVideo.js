var keystone = require('keystone');
var Types = keystone.Field.Types;
var VimeoVideo = new keystone.List('VimeoVideo', {
    map: { name: 'title' },
    autokey: { path: 'key', from: 'title', unique: true }
});

VimeoVideo.add({
    featured: { type: Boolean },
    title: { type: String, initial: true, required: true, index: true },
    subtitle: { type: String },
    description: { type: Types.Textarea },
    url: { type: String },
    thumbnail: { type: String },
    foreignId: { type: String },
    uploadDate: { type: Types.Datetime }, 
    lastUpdate: { type: Types.Datetime } 
});

VimeoVideo.defaultColumns = 'title, featured';
VimeoVideo.register();
