var keystone = require('keystone');
var Types = keystone.Field.Types;
var StandingVoice = new keystone.List('StandingVoice', {
    autokey: { path: 'key', from: 'name', unique: true },
});

StandingVoice.add({
    name: { type: String, initial: true, required: true, index: true },
    role: { type: String, initial: true, required: true }, 
    previewImage: { type: Types.CloudinaryImage, autoCleanup: true },
    galleryImages: { type: Types.CloudinaryImages, autoCleanup: true },
    content: { 
        introduction: { type: Types.Markdown },
        body: { type: Types.Markdown }
    },
	url: { type: String	}
});

StandingVoice.register();
