var keystone = require('keystone');
var Types = keystone.Field.Types;
var FundraisingCallToAction = new keystone.List('FundraisingCallToAction', {
	autokey: { path: 'key', from: 'title', unique: true },
	map: { name: 'title' }
});

FundraisingCallToAction.add({
	title: { type: String, required: true, index: true, initial: true },
	quote: { type: Types.Text, required: true, initial: true },
	text: { type: Types.Html, wysiwyg: true, required: true, initial: true },
	buttonText: { type: Types.Text, required: true, initial: true },
	enabled: { type: Boolean }
});

FundraisingCallToAction.register();
