var keystone = require('keystone');
var Types = keystone.Field.Types;
var TeamMember = new keystone.List('TeamMember', {
    sortable: true,
    defaultSort: 'sortOrder',
    autokey: { path: 'key', from: 'name', unique: true },
});

TeamMember.add({
    name: { type: Types.Name, initial: true, required: true, index: true },
    title: { type: String },
    email: { type: Types.Email, displayGravatar: true },
    biography: { type: Types.Markdown },
    team: { type: Types.Relationship, ref: 'Team' },
    image: { type: Types.CloudinaryImage, autoCleanup: true }
});

TeamMember.register();
