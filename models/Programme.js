var keystone = require('keystone');
var Types = keystone.Field.Types;
var Programme = new keystone.List('Programme', {
    sortable: true,
    defaultSort: 'sortOrder',
    autokey: { path: 'key', from: 'title', unique: true },
    map: { name: 'title' }
});

Programme.add({
    icon: { type: Types.CloudinaryImage, autoCleanup: true },
    title: { type: String, required: true, index: true, initial: true },
    overview: { type: Types.Textarea, height: 80 },
    category: { type: Types.Select, required: true, initial: true, options: 'health, advocacy, community, none', default: 'none' }
},
'Introduction',
{
    introduction: {
        title: { type: String, height: 80 },
        content: { type: Types.Markdown, height: 360 },
        pullQuote: { type: Types.Textarea, height: 80 },
        headerImages: { type: Types.CloudinaryImages, autoCleanup: true, folder: 'programme-headers' }
    }
},
'Data',
{
    data: {
        title: { type: String },
        summary: { type: Types.Textarea, height: 80 },
        percentage: { type: Number },
        percentageTitle: { type: String },
        percentageDescription: { type: Types.Textarea, height: 80 },
        comparisonPre: { type: Number },
        comparisonPost: { type: Number },
        comparisonTitle: { type: String },
        comparisonDescription: { type: Types.Textarea, height: 80 },
        fixed: { type: Number },
        fixedTitle: { type: String },
        fixedDescription: { type: Types.Textarea, height: 80 }
    }
},
'Icons',
{
    icons: {
        one: {
            icon: { type: String, note: 'Copy an icon name from the <a href="http://fortawesome.github.io/Font-Awesome/icons/">icon list</a>' },
            value: { type: Number },
            title: { type: String },
            description: { type: Types.Textarea, height: 80 }
        },
        two: {
            icon: { type: String, note: 'Copy an icon name from the <a href="http://fortawesome.github.io/Font-Awesome/icons/">icon list</a>' },
            value: { type: Number },
            title: { type: String },
            description: { type: Types.Textarea, height: 80 }
        },
        three: {
            icon: { type: String, note: 'Copy an icon name from the <a href="http://fortawesome.github.io/Font-Awesome/icons/">icon list</a>' },
            value: { type: Number },
            title: { type: String },
            description: { type: Types.Textarea, height: 80 }
        }
    }
},
'Main Content',
{
    sectionOne: {
        title: { type: String },
        content: { type: Types.Markdown, height: 360 }
    },
    sectionTwo: {
        title: { type: String},
        content: { type: Types.Markdown, height: 360 }
    },
    sectionThree: {
        title: { type: String},
        content: { type: Types.Markdown, height: 360 }
    }
},
'Key Facts',
{
    factOne: {
        title: { type: String },
        content: { type: String }
    },
    factTwo: {
        title: { type: String },
        content: { type: String }
    }
},
'Case Studies',
{
    csMeta: {
        title: { type: String },
        introduction: { type: Types.Textarea }
    }
},
'Quotes',
{
    quote: {
        one: {
            image: { type: Types.CloudinaryImage, autoCleanup: true, folder: 'quote-context' },
            content: { type: String },
            attribution: { type: String },
            featured: { type: Boolean }
        },
        two: {
            image: { type: Types.CloudinaryImage, autoCleanup: true, folder: 'quote-context' },
            content: { type: String },
            attribution: { type: String },
            featured: { type: Boolean }
        },
        three: {
            image: { type: Types.CloudinaryImage, autoCleanup: true, folder: 'quote-context' },
            content: { type: String },
            attribution: { type: String },
            featured: { type: Boolean }
        },
        four: {
            image: { type: Types.CloudinaryImage, autoCleanup: true, folder: 'quote-context' },
            content: { type: String },
            attribution: { type: String },
            featured: { type: Boolean }
        }
    }
},
'Closing',
{
    closing: {
        image: { type: Types.CloudinaryImage, autoCleanup: true, folder: 'parallax-backgrounds' },
        heading: { type: String },
        content: { type: Types.Textarea }
    }
}
);

Programme.relationship({
    path: 'caseStudies', ref: 'CaseStudy', refPath: 'programme'
});

Programme.defaultColumns = "title, category";

Programme.register();
