module.exports = {
	debug: {
		script: 'keystone.js',
		options: {
			nodeArgs: ['--inspect'],
			env: {
				port: 3000
			}
		}
	}
};
