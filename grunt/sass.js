var sass = require('node-sass');

module.exports = {
	sass: {
		options: {
			implementation: sass,
			style: 'expanded'
		},
		dist: {
			files: {
				'public/styles/site.css': 'public/styles/site.scss'
			}
		}
	}
};
