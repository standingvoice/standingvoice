var UI = UI || {};

UI.binds = (function($, window, document, undefined) {

    'use strict';

    var foobar = 'fooo',
        controller = new ScrollMagic.Controller(),
        parallax_controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});


    var testConsole = function() {},   //not required
   selectMenus = function(){

      $('.mobileselect').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });

   },
   quoteCarousel = function(){

        $('.svquote__content').slick({
           dots: true,
           infinite: true,
           speed: 500,
           fade: true,
           arrows: false,
           autoplay: true,
           cssEase: 'linear',
           autoplaySpeed: 7000,
           pauseOnHover: false,
           asNavFor: '.quoteImage-carousel'
        });


        $('.quoteImage-carousel').slick({
           dots: true,
           infinite: true,
           speed: 500,
           fade: true,
           arrows: false,
           autoplay: true,
           cssEase: 'linear',
           pauseOnHover: false,
           autoplaySpeed: 7000,
           asNavFor: '.quoteImage-carousel'
        });

   },

   scrollToD = function(){

      $('[data-scroller]').on('click', function(e){

         e.preventDefault();
         var d = $(this).attr('data-scroller');

         $(window).scrollTo('#' + d, 500, {
            offset: -100
         });

      });


   },

   headerCarousel = function(){

      $('.headerCarousel').slick({
         dots: true,
         infinite: true,
         speed: 800,
         arrows: true,
         autoplaySpeed: 4000,
         autoplay: true,
         cssEase: 'cubic-bezier(0.9,0,0.7,1)',
         pauseOnHover: true,
         prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
         nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>'
      });

   },

   programmeCarousel = function(){

      $('.headerCarousel').slick({
         dots: false,
         infinite: true,
         speed: 800,
         arrows: false,
         autoplaySpeed: 4000,
         autoplay: true,
         pauseOnHover: false,
         cssEase: 'cubic-bezier(0.9,0,0.7,1)'
      });

   },

   programmeHover = function(){


      $('#jsProgGridHook').on('mouseover', '.programmegrid__wrapper--type', function(){

         var $this = $(this);

         $this.find('img').eq(0).addClass('hidden');
         $this.find('img').eq(1).removeClass('hidden');

      });



      $('#jsProgGridHook').on('mouseleave', '.programmegrid__wrapper--type', function(){

         var $this = $(this);

         $this.find('img').eq(0).removeClass('hidden');
         $this.find('img').eq(1).addClass('hidden');

      });

      $('#jsProgGridHook').on('click', '.programmegrid__wrapper--type', function(){

         var _url = $(this).attr('data-url');

         window.open(_url,"_self");

      });


   },
   svgReplace = function(){


      jQuery('img.svg').each(function(){
         var $img = jQuery(this);
         var imgID = $img.attr('id');
         var imgClass = $img.attr('class');
         var imgURL = $img.attr('src');

         jQuery.get(imgURL, function(data) {
             // Get the SVG tag, ignore the rest
             var $svg = jQuery(data).find('svg');

             // Add replaced image's ID to the new SVG
             if(typeof imgID !== 'undefined') {
                 $svg = $svg.attr('id', imgID);
             }
             // Add replaced image's classes to the new SVG
             if(typeof imgClass !== 'undefined') {
                 $svg = $svg.attr('class', imgClass+' replaced-svg');
             }

             // Remove any invalid XML tags as per http://validator.w3.org
             $svg = $svg.removeAttr('xmlns:a');

             // Replace image with new SVG
             $img.replaceWith($svg);

         }, 'xml');

     });


   },
   shareButtonUrls = function(){

      //now placed on programme and resources page seperately

   },
   chartTypes = function(){


      //get data attributes from charts
      //chart one
      var percentageVal = $('#chartTypeOne').attr('data-pct');

      //chart two
      var BarVal1 = $('#chartTypeTwo').attr('data-pre');
      var BarVal2 = $('#chartTypeTwo').attr('data-post');

      //chart three
      var FixedVal = $('#chartTypeThree').attr('data-fixed');


      //global graph options - turning off scales
      Chart.defaults.global.responsive = false;
      Chart.defaults.global.tooltipTitleFontFamily = "'effra', Helvetica, Roboto, Arial, sans-serif";
      Chart.defaults.global.scaleShowLabels = false;
      Chart.defaults.global.showScale = false;
      Chart.defaults.global.scaleLineWidth = 0;
      Chart.defaults.global.showTooltips = false;


      var data = [
          {
              value: percentageVal,
              color:"rgba(255,255,255,1)",
              highlight: "rgba(255,255,255,1)"
          },
          {
              value: (100 - percentageVal),
              color: "rgba(255,255,255,0.6)",
              highlight: "rgba(255,255,255,0.6)"
          }
      ]


      var data2 = {
          labels: ["type_1"],
          datasets: [
              {
                  label: "My First dataset",
                  fillColor: "rgba(255,255,255,0.6)",
                  highlightFill: "rgba(255,255,255,0.6)",
                  data: [BarVal1],
                  barValueSpacing : 5
              },
              {
                  label: "My Second dataset",
                  fillColor: "rgba(255,255,255,1)",
                  highlightFill: "rgba(255,255,255,1)",
                  data: [BarVal2],
                  barValueSpacing : 5
              }
          ]
      };



      var data3 = {
          labels: ["type_1", "type_2", "type_3"],
          datasets: [
              {
                  fillColor: "rgba(255,255,255,0.6)",
                  pointColor: "rgba(255,255,255,1)",
                  highlightFill: "rgba(255,255,255,1)",
                  strokeColor: "rgba(255,255,255,1)",
                  data: [10, 45, 100]
              }
          ]
      };





      //triggering the graphs
      var scene5 = new ScrollMagic.Scene({triggerElement: "#jsTriggerStat", duration: 600})
                  .offset(0)
                  .addTo(controller);




      //-------remove controllers based on screen size



      function graphsOn(){

         if($('#jsStatsHook').hasClass('graphs-on') !== true) {

            if($('#chartTypeOne').length) {
               var ctx = document.getElementById("chartTypeOne").getContext("2d");
               var DoughnutChartOne = new Chart(ctx).Doughnut(data, {

                  animateRotate : true,
                  percentageInnerCutout : 70,
                  segmentShowStroke : false,
                  animateScale :false,
                  animationEasing : "easeOutSine",

               });
            }

            if($('#chartTypeTwo').length) {
               var ctx2 = document.getElementById("chartTypeTwo").getContext("2d");
               var barChart = new Chart(ctx2).Bar(data2, {

                  scaleBeginAtZero : true,
                  scaleShowHorizontalLines: false,
                  scaleShowVerticalLines: false,
                  barShowStroke : false,
                  scaleShowGridLines : false,

               });
            }

            if($('#chartTypeThree').length) {
               var ctx3 = document.getElementById("chartTypeThree").getContext("2d");
               var barChart = new Chart(ctx3).Line(data3, {

                  scaleBeginAtZero : true,
                  scaleShowHorizontalLines: false,
                  scaleShowVerticalLines: false,
                  barShowStroke : false,
                  scaleShowGridLines : false,

               });
            }

            $('#jsStatsHook').addClass('graphs-on'); //add class to stop graphs re-animating

         }

      }



      if (/Mobi/.test(navigator.userAgent)) {

         graphsOn();

      } else {

         graphsOn();

      }

   },
   semanticAccordion = function(){


      $('.ui.accordion').accordion();

      var r=0,
          s=0;

      $('.teammember').each(function(){
         r += $(this).outerHeight();
      });

      $('.linetitle__region').each(function(){
         s += $(this).outerHeight() + 100;
      });

      var x = $('.teammember').outerHeight(),
          y = $('.teammember').length,
          b = $('.section__white').outerHeight(),
          k = $('#jsAccordion').outerHeight(),
          m = $('#jsAccordion').find('.content').last().find('li').length,
          n = $('#jsAccordion').find('.content').last().outerHeight(),
          z = (r+s)-(n+k);


          var accordionPin = new ScrollMagic.Scene({triggerElement: "#accordiontrigger", duration: z})
                       .setPin("#jsAccordion", {
                           pushFollowers: false
                       })
                       .offset(320)
                       .addTo(controller);



         var winwidth = $(window).width();

         function fixStickyElements(bool){

            if(bool == true) {

               accordionPin.remove();
               accordionPin.removePin(true);
               $('#jsAccordion').prependTo('#jsFacesHook');


            } else if(bool == false) {


               if($('#accordiontrigger').next().hasClass('scrollmagic-pin-spacer') !== true) {

                  $('#jsAccordion').insertAfter('#accordiontrigger');
                  accordionPin.refresh();
                  accordionPin.setPin("#jsAccordion");

               }

            }

         }



         //----stopping all sticky elements on init...
         if(winwidth <= 640) {
            fixStickyElements(true);
         } else {
            fixStickyElements(false);
         }


         //----stopping all sticky elements on resize...
         $(window).resize(function() {
            var l = $(window).width();

            if(l <= 640) {
               fixStickyElements(true);
            } else {
               fixStickyElements(false);
            }
         });


         if (/Mobi/.test(navigator.userAgent)) {

               controller.enabled(false);
               $('#jsAccordion').prependTo('.section__white').wrap( "<div class='row'><div class='large-12 columns'></div></div>" ).css('margin-top', '20px');

         } else {


          //TODO iterate through these properly

          var markers = [],
              regHeights = [];

          for (var i = 0; i < $('.linetitle__region--divider').length; ++i) {
              markers[i] = 'teamhook_' + i;
          }

          $('.region--container').each(function(e){
             regHeights[e] = $(this).height();
          });



          $('.linetitle__region').each(function(e){

             var el = $(this).attr('id');

             //create subsequent markers for section durations

             markers[e] = new ScrollMagic.Scene({triggerElement: '#' + el, duration: regHeights[e]})
                         .offset(200)
                         .addTo(controller);

             markers[e].on("enter", function (event) {

                $('#jsAccordion').accordion('open', e);

             });

          });


          accordionPin.on("leave", function (event) {

             $('#jsAccordion').accordion('close', 0);

          });

       }



   },
   sidefactsPin = function(){

      var x = $('#jsProgrammeContent').outerHeight(),
          y = $('#jsSidefacts').outerHeight();


    var accordionPin = new ScrollMagic.Scene({triggerElement: "#sidefactsTrigger", duration: (x-y-50)})
                .setPin("#jsSidefacts", {
                     pushFollowers: false
                 })
                 .offset(300) //was 340
                .addTo(controller);


      var k = $(window).width();

      function fixStickyElements(bool){

         if(bool === true) {

            accordionPin.remove();
            accordionPin.removePin(true);
            $('#jsSidefacts').prependTo('#jsProgrammeContent');

         } else if(bool === false) {


            if($('#sidefactsTrigger').next().hasClass('scrollmagic-pin-spacer') !== true) {

               $('#jsSidefacts').insertAfter('#sidefactsTrigger');
               accordionPin.refresh();
               accordionPin.setPin("#jsSidefacts");

            }

         }

      }


      //----stopping all sticky elements on init...
      if(k <= 1024) {
         fixStickyElements(true);
      } else {
         fixStickyElements(false);
      }


      //----stopping all sticky elements on resize...
      $(window).resize(function() {
         var l = $(window).width();

         if(l <= 1024) {
            fixStickyElements(true);
         } else {
            fixStickyElements(false);
         }
      });


   },
   generalHoverStateCasestudies = function(){

      $('#jsCaseStudiesHook').on('mouseover', '.casestudies__container--image', function(e){

         e.stopPropagation();

         $(this).find('img').velocity("stop", true).velocity({
            opacity : 0.5
         }, 200, 'easeInSine');

         $(this).find('.viewoverlay').velocity("stop", true).velocity({
            marginTop : '0px',
            opacity : 1
         }, 200, 'easeInSine');

         //e.stopPropagation();
      });

      $('#jsCaseStudiesHook').on('mouseleave', '.casestudies__container--image', function(e){

         e.stopPropagation();

         $(this).find('img').velocity("stop", true).velocity({
            opacity : 1
         }, 200, 'easeInSine');


         $(this).find('.viewoverlay').velocity("stop", true).velocity({
            marginTop : '7px',
            opacity : 0
         }, 350, 'easeInSine');

         //e.stopPropagation();
      });


   },
   generalHoverStateVoices = function(){

      $('#jsVoicesGrid').on('mouseover', '.voicesblocks__grid--voice', function(e){

         e.stopPropagation();

         $(this).find('img').velocity("stop", true).velocity({
            opacity : 0.7
         }, 200, 'easeInSine');

         $(this).find('.viewoverlay').velocity("stop", true).velocity({
            marginTop : '0px',
            opacity : 1
         }, 350, 'easeInSine');

      });

      $('#jsVoicesGrid').on('mouseleave', '.voicesblocks__grid--voice', function(e){

         e.stopPropagation();

         $(this).find('img').velocity("stop", true).velocity({
            opacity : 1
         }, 200, 'easeInSine');

         $(this).find('.viewoverlay').velocity("stop", true).velocity({
            marginTop : '10px',
            opacity : 0
         }, 350, 'easeInSine');

      });


   },
   parallaxSection = function(){
      $(function () {

        // build scenes
         new ScrollMagic.Scene({triggerElement: "#parallax1"})
          .setTween("#parallax1 > div", {y: "70%", ease: Linear.easeNone})
          .addTo(parallax_controller);

      });

   },
   headerParallaxSection = function(){
      $(function () {

        // build scenes
         new ScrollMagic.Scene({triggerElement: "#parallax2"})
          .setTween("#parallax2 > div", {y: "70%", ease: Linear.easeNone})
          .addTo(parallax_controller);

      });

   },
   parallaxCopy = function(){

      // build tween
    var tween = new TimelineMax ()
      .add([
        TweenMax.fromTo("#jsParallaxHook .svparallax__copy", 1, {top: 250}, {top: -50, ease: Linear.easeNone})
      ]);

    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: "#parallax1", duration: $(window).width()})
            .setTween(tween)
            .addTo(controller);

   },
   navUX = function(){

      $('#nav-icon3').click(function(){

         $(this).toggleClass('open');
         $('#jsOffCanvasHook').addClass('move-left');

      });

      $(document).on('close.fndtn.offcanvas', '[data-offcanvas]', function () {
        $('#nav-icon3').removeClass('open');
      });


   },
   stickynav = function(){

      $(function () { // wait for document ready
         // build scene
         var y = $(window).width();

         function fixStickyElements(bool){

            if(bool === true) {

               scene.remove();
               scene.removePin(true);
               scene2.remove();
               scene2.removePin(true);
               scene3.remove();
               scene3.removePin(true);
               $('#jsStickyBgHook').hide();
               $('#jsMarkHook').hide();

            } else if(bool === false) {

               $('#jsStickyBgHook').show();
               $('#jsMarkHook').show();
               scene.refresh();
               scene.setPin('#jsNavigationHook');
               scene2.refresh();
               scene2.setPin('#jsStickyBgHook');
               scene3.refresh();
               scene3.setPin('#jsMarkHook');

            }

         }

         //----stopping all sticky elements on touch devices...
         if (/Mobi/.test(navigator.userAgent)) {
            controller.enabled(false);
            parallax_controller.enabled(false);
         }



         //TODO consider doing this with tweening rather than timed animation
         var offsetval = 25, //135
            duration_val = $(document).height() - 1000;

         //offset 390
         var scene = new ScrollMagic.Scene({duration: duration_val})
                     .setPin("#jsNavigationHook", {
                          pushFollowers: false
                      })
                     .offset(offsetval)
                     .addTo(controller);

         //offset 390
         var scene2 = new ScrollMagic.Scene({duration: duration_val})
                     .setPin("#jsStickyBgHook", {
                          pushFollowers: false
                      })
                     .offset(offsetval)
                     .addTo(controller);

         //offset 360
         var scene3 = new ScrollMagic.Scene({duration: duration_val})
                     .setPin("#jsMarkHook", {
                          pushFollowers: false
                      })
                     .offset(offsetval)
                     .addTo(controller);

         //offset 500
         var scene4 = new ScrollMagic.Scene({duration: duration_val})
                     .offset(130)
                     .addTo(controller);




         //----stopping all sticky elements on init...
         if(y <= 1024) {
            fixStickyElements(true);
         } else {
            fixStickyElements(false);
         }


         //----stopping all sticky elements on resize...
         $(window).resize(function() {
            var x = $(window).width();

            if(x <= 1024) {
               fixStickyElements(true);
            } else {
               fixStickyElements(false);
            }
         });




         scene.on("enter", function (event) {
            $('.sticky-background').velocity({
               height: '100px',
               opacity: 1
            }, 200, 'easeOutSine');
         });

         scene.on("leave", function (event) {
            console.log(event);

           if(event.scrollDirection !== 'FORWARD') {

             if($('#jsStickyBgHook').hasClass('fullbleed')) {

                $('.sticky-background').velocity({
                    height: '100px',
                    opacity: 0
                 }, 200, 'easeOutSine');

             } else {

                $('.sticky-background').velocity({
                    height: '100px',
                    opacity: 0   //change afterwards
                 }, 200, 'easeOutSine');

             }

           } else if(event.scrollDirection == 'FORWARD') {}

         });

         scene4.on("enter", function (event) {
            $('#jsMarkHook').find('img').transition('scale');
         });

         scene4.on("leave", function (event) {
            $('#jsMarkHook').find('img').transition('scale', '200ms');
         });


      });

   },
   semanticTitleAnimate = function(){

      //animated titles on all pages using ui Transitions
      $('#jsTitleHook').find('.title__container--main').transition('fade up', 500, function(){
         $('#jsTitleHook').find('.title__container--sub').transition('fade up', 500, function(){});
      });

   },
   semanticDropdownNav = function(){

      $('.ui.dropdown').dropdown({
         action : 'nothing'
      });

   },
   vimeo = function() {

      var $current = $('.selected'),
          _firsttitle = $current.attr('data-title'),
          _firstdesc = $current.attr('data-description'),
          _firstid = $current.attr('data-id');

       $('#twbtn').ShareLink({
          title: _firsttitle,
          text: _firstdesc,
          image: 'https://vimeo.com/' + _firstid,
          url: 'https://vimeo.com/' + _firstid
       });

      $('.vimeo-video').on('click', function(e) {
        var iframe = $('#player--iframe'),
            desc = $('#player--desc'),
            title = $('#player--title'),
            _id = $(this).data('id'),
            _src = 'https://player.vimeo.com/video/' + _id,
            _desc = $(this).data('description'),
            _title = $(this).data('title'),
            speed = 200;

         $(window).scrollTo('#jsScrollToHook', 500, {
            offset: -70
         });

        $('.vimeo-video.selected').removeClass('selected');
        $(this).addClass('selected');

        iframe.attr('src', _src);
        iframe.fadeOut(speed, function() {
          iframe.fadeIn(speed);
       });

        desc.fadeOut(speed, function() {
          desc.html(_desc);
          desc.fadeIn(speed);
        });

        title.fadeOut(speed, function() {
          title.html(_title);
          title.fadeIn(speed);
        });


        $('#twbtn').ShareLink({
           title: _title,
           text: _desc,
           image: 'https://vimeo.com/' + _id,
           url: 'https://vimeo.com/' + _id
        });

        return desc, title;

        e.preventDefault();
      });
   };
   //public
    return {

      testConsole : testConsole,
      semanticDropdownNav : semanticDropdownNav,
      stickynav : stickynav,
      parallaxSection : parallaxSection,
      headerParallaxSection : headerParallaxSection,
      semanticTitleAnimate : semanticTitleAnimate,
      navUX : navUX,
      parallaxCopy : parallaxCopy,
      chartTypes : chartTypes,
      semanticAccordion : semanticAccordion,
      vimeo : vimeo,
      svgReplace : svgReplace,
      quoteCarousel : quoteCarousel,
      generalHoverStateVoices: generalHoverStateVoices,
      generalHoverStateCasestudies: generalHoverStateCasestudies,
      programmeHover: programmeHover,
      headerCarousel : headerCarousel,
      sidefactsPin : sidefactsPin,
      scrollToD : scrollToD,
      shareButtonUrls : shareButtonUrls,
      programmeCarousel : programmeCarousel,
      selectMenus : selectMenus

    };



})(jQuery, window, document);
