var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initProgrammes);
keystone.pre('routes', middleware.initFooterDate);
keystone.pre('routes', middleware.initOfficeAddresses);

// Import Route Controllers
var routes = {
    views: importRoutes('./views'),
    api: importRoutes('./api')
};

// Setup Route Bindings
exports = module.exports = function(app) {

    // Views
    if (keystone.get('env') === 'production') {
      app.get('/', middleware.rewrites.ssl, routes.views.index);
      app.get('/programmes', middleware.rewrites.ssl, routes.views.programmes);
      app.get('/programmes/:programme?', middleware.rewrites.ssl, routes.views.programme);
      app.get('/about', middleware.rewrites.ssl, routes.views.about);
      app.get('/standing-voices', middleware.rewrites.ssl, routes.views.voices);
      app.get('/standing-voices/:voice?', middleware.rewrites.ssl, routes.views.voice);
      app.get('/resources', middleware.rewrites.ssl, routes.views.resources);
      app.get('/fundraising', middleware.rewrites.ssl, routes.views.fundraising);
      app.get('/donations', middleware.rewrites.ssl, routes.views.donations);
      app.get('/donations/donate/:cause?', middleware.rewrites.ssl, routes.views.donate);
      app.get('/albinism', middleware.rewrites.ssl, routes.views.albinism);
      app.get('/health', middleware.rewrites.ssl, routes.views.health);
      app.get('/advocacy', middleware.rewrites.ssl, routes.views.advocacy);
      app.get('/community', middleware.rewrites.ssl, routes.views.community);
    } else {
      app.get('/', routes.views.index);
      app.get('/programmes', routes.views.programmes);
      app.get('/programmes/:programme?', routes.views.programme);
      app.get('/about', routes.views.about);
      app.get('/standing-voices', routes.views.voices);
      app.get('/standing-voices/:voice?', routes.views.voice);
      app.get('/resources', routes.views.resources);
      app.get('/fundraising', routes.views.fundraising);
      app.get('/donations', routes.views.donations);
      app.get('/donations/donate/:cause?', routes.views.donate);
      app.get('/albinism', routes.views.albinism);
      app.get('/health', routes.views.health);
      app.get('/advocacy', routes.views.advocacy);
      app.get('/community', routes.views.community);
    }

    // API
    app.post('/api/donate', keystone.middleware.api, routes.api.donate);
};
