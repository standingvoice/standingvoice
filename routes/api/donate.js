var keystone = require('keystone'),
    Donor = keystone.list('Donor').model,
    stripe = require('stripe')( process.env.STRIPE_SECRET ),
	request = require('request');

var retrieveDonor = function(email, token) {
    return new Promise(function(resolve, reject) {
        // Look for an existing Donor
        Donor.findOne({
            email: email
        }, function(err, donor) {
            if (err) return reject(err);
            if (donor) return resolve(donor);

            // Create a new one if none exists
            stripe.customers.create({
                email: email,
                description: email,
                source: token
            }, function(err, customer) {
                if (err) return reject(err);
                var donor = new Donor({
                    email: email,
                    stripeId: customer.id
                });
                donor.save(function(err, donor) {
                    if (err) return reject(err);
                    resolve(donor);
                });
            });
        });
    });
}

var chargeOnce = function(amount, donor, next) {
    stripe.charges.create({
        amount: amount,
        currency: 'gbp',
        customer: donor.stripeId,
        description: 'Donation to Standing Voice'
    }, function(err, charge) {
        if (err) return next(err);
        next();
    });
};

var chargeMonthly = function(amount, donor, next) {
    stripe.subscriptions.create(
		{
			customer: donor.stripeId,
			items: [
				{
					plan: 'monthly-donation',
					quantity: amount
				}
			]
		},
        function(err, subscription) {
            if (err) return next(err);
            next();
        }
    );
};

var buildVerifyRecaptchaUrl = function(token) {
	return 'https://www.google.com/recaptcha/api/siteverify?secret=' + process.env.RECAPTCHA_SECRET_KEY + '&response=' + token;
};

exports = module.exports = function(req, res) {
    var view = new keystone.View(req, res),
        locals = res.locals;

    locals.data = {};

    view.on('init', function(next) {
        var amount  = req.body.amount,
            email   = req.body.token.email,
            token   = req.body.token.id,
            monthly = req.body.monthly;

		var verifyUrlWithToken = buildVerifyRecaptchaUrl(req.body.recaptchaToken);

		request.post(verifyUrlWithToken, function(err, response) {
			var body = JSON.parse(response.body);
			
			if (body.success && body.score > +process.env.RECAPTCHA_THRESHHOLD) {
				retrieveDonor(
					email,
					token
				).then(function(donor) {
					if (monthly === 'true') {
						chargeMonthly(
							amount,
							donor,
							next
						);
					}
					else {
						chargeOnce(
							amount,
							donor,
							next
						);
					}
				}).catch(function(err) {
					next(err);
				});
			} else {
				console.log('reCAPTCHA verification did not succeed');
				locals.data.recaptchaError = true;
				next(err);
			}
		});        
    });

    view.render(function(err) {
        if (err) return res.apiError('error', err);
        res.apiResponse(locals.data);
    });
};
