var keystone = require('keystone');
var _ = require('underscore');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};
    locals.section = 'about';

    locals.meta = {
      og: {
        type:  'article',
        title: 'About Standing Voice',
        url:   '/about',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'About Standing Voice',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {
        keystone
          .list('TextSnippet')
          .model
          .find()
          .exec(function(err, snippets){
            locals.snippets = _.indexBy(snippets, 'key');
            next(err);
          });
    });

    view.on('init', function(next) {
        keystone
            .list('Team')
            .model
            .find()
			.sort('sortOrder')
			.exec(function(err, teams) {
                keystone.populateRelated(
                    teams, 
                    'members', 
                    function(err) {
                        locals.data.teams = teams;
                        next(err)
                    }
                );
            });
    });

    view.render('about');
    
};
