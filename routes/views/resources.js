var keystone = require('keystone'),
    request = require('request'),
    moment = require('moment'),
    VimeoVideo = keystone.list('VimeoVideo').model,
    Images = keystone.list('Image').model,
    Files = keystone.list('File').model,
    path = require('path');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.section = 'resources';

    locals.data = {
        featured: null,
        videos: [],
        files: {}
    };

    locals.meta = {
      og: {
        type:  'article',
        title: 'Resources',
        url:   '/resources',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Resources',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    // // Vimeo API cache
    // view.on('init', function(next) {

    //     // Get the most recent video from the db
    //     VimeoVideo
    //         .findOne()
    //         .sort('-lastUpdate')
    //         .exec(function(err, video) {

    //             // If there isn't one or the cache has expired
    //             if (!video || video.lastUpdate.getTime() < moment() - 60000) {
    //                 var opts = { url: "http://vimeo.com/api/v2/user/user5961579/videos.json" };
    //                 request(opts, function(err, res, body) {
    //                     var videos = JSON.parse(body),
    //                         fresh = 0;

    //                     for (var i = 0; i < videos.length; i++) {
            
    //                         // Bail on first common video
    //                         if (video && video.foreignId == videos[i].id) {
    //                             break;
    //                         }

    //                         var item = new VimeoVideo(),
    //                             data = {
    //                                 foreignId: videos[i].id,
    //                                 title: videos[i].title,
    //                                 subtitle: videos[i].description,
    //                                 description: videos[i].description,
    //                                 url: videos[i].url,
    //                                 thumbnail: videos[i].thumbnail_large,
    //                                 uploadDate: moment(videos[i].upload_date).format('YYYY-MM-DD hh:mm:ss a')
    //                             };

    //                         if (i === 0) {
    //                             data.lastUpdate = moment().format('YYYY-MM-DD hh:mm:ss a');
    //                         }

    //                         item.getUpdateHandler(req).process(data);
    //                         fresh++;
    //                     }
    //                     // Update the most recent local video
    //                     if (fresh === 0) {
                        
    //                         video.getUpdateHandler(req).process({
    //                             lastUpdate: moment().format('YYYY-MM-DD hh:mm:ss a')
    //                         }, function(err) {
    //                             next(err);
    //                         });  
                        
    //                     } else {
    //                         next(err);
    //                     }

    //                 });

    //             } else {
    //                next(err);  
    //             }
    //     });

    // });

    view.on('init', function(next) {
        // Load one featured video
        VimeoVideo
            .findOne()
            .where('featured', true)
            .exec(function(err, featuredVideo) {
                locals.data.featured = featuredVideo;

                // Load the rest
                VimeoVideo
                    .find()
                    .sort('uploadDate')
                    .exec(function(err, videos) {
                        if (videos && !featuredVideo && videos[0]) {
                            locals.data.featured = videos[0];
                        }

                        for (var i = 0; i < videos.length; i++) {
                            if (videos[i].id === locals.data.featured.id) {
                                videos[i].initial = true;
                                break;
                            }
                        }

                        locals.data.videos = videos;
                        next(err);
                    });
            });
    });

    view.on('init', function(next) {
        // Load images
        Images
            .find(function(err, images) {
                locals.data.images = images;
                next(err);
            });
    });

    view.on('init', function(next) {
        Files
            .find()
            .exec(function(err, assets) {
                for (var i = 0; i < assets.length; i++) {
                    if (locals.data.files[assets[i].usage] == undefined) {
                        locals.data.files[assets[i].usage] = [];
                    }
                    assets[i].ext = path.extname(assets[i].file.filename).replace('.', '');
                    locals.data.files[assets[i].usage].push(assets[i]);
                }
                next(err);
            });
    });

    view.render('resources');
    
};
