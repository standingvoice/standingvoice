var keystone = require('keystone');
var _ = require('underscore');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.section = 'home';

    locals.meta = {
      og: {
        type:  'article',
        title: 'Standing Voice',
        url:   '/',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Standing Voice',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {
        keystone
          .list('TextSnippet')
          .model
          .find()
          .exec(function(err, snippets){
              locals.snippets = _.indexBy(snippets, 'key');
              next(err);
          });
    });

    view.on('init', function(next) {
        keystone
          .list('Marker')
          .model
          .find()
          .exec(function(err, markers){
              locals.markers = markers;
              next(err);
          });
    });

	view.on('init', function(next) {
		keystone
			.list('HomepageBanner')
			.model
			.find()
			.exec(function(err, banners){
				banners.forEach(function(banner) {
					if (banner.enabled) {
						locals.banner = banner.content;
					}
				});
				
				next(err);
			});
	});

	view.on('init', function(next) {
		keystone
			.list('MapStyle')
			.model
			.find()
			.exec(function(err, mapStyle){
				locals.mapStyle = mapStyle[0].content || '[{}]';
				next(err);
			});
	});
    
    view.render('index');

};
