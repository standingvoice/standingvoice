var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};
    locals.section = 'donate';

    
    locals.meta = {
      og: {
        type:  'article',
        title: 'Donate',
        url:   '/donate',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Donate',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    if (req.params.cause) {
        view.on('init', function(next) {
            keystone
                .list('DonationCause')
                .model
                .findOne({
                    key: req.params.cause
                })
                .exec(function(err, result) {
                    if (result) { 
                        locals.data.donationCause = result;
                    }
                    next(err);
                });
        });
    }

    if (req.query.amount) {
        locals.data.donationAmount = req.query.amount;
    }

    view.render('donate');
    
};
