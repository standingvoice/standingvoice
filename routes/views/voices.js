var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.section = 'voices';
    locals.data = {};
    
    locals.meta = {
      og: {
        type:  'article',
        title: 'Standing Voices',
        url:   '/standing-voices',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Standing Voices',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {
        keystone
            .list('StandingVoice')
            .model
            .find(function(err, results) {
                if (results) { 
                    locals.data.voices = results;
                }
                next(err);
            });
    });  
    
    view.on('init', function(next) {
		keystone
			.list('StandingVoicesContent')
			.model
			.find(function(err, results) {
				if (results && results[0]) {
					locals.data.topSection = results[0].topSection;
					locals.data.bottomSection = results[0].bottomSection;
					locals.data.middleImage = results[0].middleImage.url;
				}
				next(err);
			});
    });

    view.render('voices');
};
