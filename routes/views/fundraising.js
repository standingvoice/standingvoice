var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};


    locals.meta = {
      og: {
        type:  'article',
        title: 'Fundraising',
        url:   '/fundraising',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Fundraising',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {
        
        keystone
            .list('Carousel')
            .model
            .findOne({
                _id: '55f2b5b998d3481ea54fff29' 
            })
            .exec(function(err, carousel) {
                locals.data.carousel = carousel;
                next(err);
            });

    });

    view.on('init', function(next) {

        keystone
            .list('FundraisingCategory')
            .model
            .find()
            .exec(function(err, categories) {
                keystone.populateRelated(categories, 'fundraisers', function(err) {
                    locals.data.fundraisingCategories = categories;
                    next(err);
                });
            });

    });

    view.on('init', function(next) {

        keystone
            .list('Event')
            .model
            .find()
            .sort('date')
            .exec(function(err, events) {
                locals.data.events = events;
                next(err);
            });

    });

	view.on('init', function(next) {

		keystone
			.list('FundraisingCallToAction')
			.model
			.find()
			.exec(function(err, callToActions) {
				var callToAction = callToActions.find(function(cta) { return cta.enabled; });
				
				if (callToAction != null) {
					locals.data.shouldShowCtaSection = true;
					locals.data.ctaQuote = callToAction.quote;
					locals.data.ctaText = callToAction.text;
					locals.data.buttonText = callToAction.buttonText;
				} else {
					locals.data.shouldShowCtaSection = false;
				}
				
				next(err);
			});

	});

    locals.section = 'fundraising';
    view.render('fundraising');
    
};
