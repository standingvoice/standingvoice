var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

    view.on('init', function(next) {

    locals.meta = {
      og: {
        type:  'article',
        title: 'Albinism',
        url:   '/albinism',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Albinism',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };
        keystone
            .list('Programme')
            .model
            .find({
                category: 'community'
            })
            .exec(function(err, programmes) {
                locals.data.programmes = programmes;
                next(err);
            });

    });

    locals.section = 'programmes';
    view.render('community');

};
