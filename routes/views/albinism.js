var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.section = 'albinism';
    locals.data = {};

    locals.meta = {
      og: {
        type:  'article',
        title: 'Albinism',
        url:   '/albinism',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Albinism',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {

        keystone
          .list('AlbinismPageSection')
          .model
          .find()
          .sort('sortOrder')
          .exec(function(err, sections){
              locals.data.sections = sections;
              next(err);
          });
    });

    view.render('albinism');
    
};
