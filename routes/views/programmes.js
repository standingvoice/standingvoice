var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {
        categories: {
            'health': [],
            'education': [],
            'kilisun': [],
            'community': [],
            'advocacy': []
        }
    };
   
    locals.meta = {
      og: {
        type:  'article',
        title: 'Programmes',
        url:   '/programmes',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Programmes',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {
        
        keystone
            .list('Carousel')
            .model
            .findOne({
                _id: '55f2b5aa98d3481ea54fff28' 
            })
            .exec(function(err, carousel) {
                locals.data.carousel = carousel;
                next(err);
            });

    });

    view.on('init', function(next) {

        keystone
            .list('CaseStudy')
            .model
            .find({ featured: true })
            .limit(3)
            .exec(function(err, caseStudies) {
                locals.data.caseStudies = caseStudies;
                next(err);
            });

    });

    view.on('init', function(next) {

        keystone
            .list('Programme')
            .model
            .find()
            .exec(function(err, results) {
                for (var i = 0; i < results.length; i++) {
                    locals.data.categories[results[i].category] = results[i];
                }
                next(err);
            });

    });

    view.on('init', function(next) {

        keystone
          .list('Programme')
          .model
          .find({ $or: [{ 'quote.one.featured': true }, { 'quote.two.featured': true }, {'quote.three.featured': true}, {'quote.four.featured': true }] })
          .exec(function(err, results){
              locals.data.quotes = [];
              for (var i = 0; i < results.length; i++) {
                  if (results[i].quote.one.featured) {
                      locals.data.quotes.push(results[i].quote.one);
                  }
                  if (results[i].quote.two.featured) {
                      locals.data.quotes.push(results[i].quote.two);
                  }
                  if (results[i].quote.three.featured) {
                      locals.data.quotes.push(results[i].quote.three);
                  }
                  if (results[i].quote.four.featured) {
                      locals.data.quotes.push(results[i].quote.four);
                  }
              }

              next(err);
          });

    });

    locals.section = 'programmes';
    view.render('programmes');

};
