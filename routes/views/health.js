var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

    
    locals.meta = {
      og: {
        type:  'article',
        title: 'Health',
        url:   '/health',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Health',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {

        keystone
            .list('Programme')
            .model
            .find({
                category: 'health'
            })
            .exec(function(err, programmes) {
                locals.data.programmes = programmes;
                next(err);
            });

    });

    locals.section = 'programmes';
    view.render('health');

};
