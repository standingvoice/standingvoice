var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {
        nav: {}
    };

    locals.meta = {
      og: {
        type:  'article',
      },
      twitter: {
      }
    };

    view.on('init', function(next) {

        keystone
            .list('Programme')
            .model
            .find()
            .sort('sortOrder')
            .exec(function(err, programmes) {
                var nav = {};
                for (var i = 0; i < programmes.length; i++) {
                    if (programmes[i].category === 'none') programmes[i].category = programmes[i].key;
                    if (programmes[i].key === req.params.programme) {
                        nav.current = programmes[i];
                        if (i !== 0 && i !== (programmes.length-1)) {
                            nav.previous = programmes[i-1];
                            nav.next = programmes[i+1];
                        }
                        else if (i == 0) {
                            nav.previous = programmes[programmes.length-1];
                            nav.next = programmes[i+1];
                        }
                        else {
                            nav.previous = programmes[i-1];
                            nav.next = programmes[0];
                        }
                    }
                }
                locals.data.nav = nav;
                next(err);
            });

    });

    view.on('init', function(next) {
        
        keystone
            .list('Programme')
            .model
            .findOne({
                key: req.params.programme
            })
            .exec(function(err, programme) {
                if (programme) {
                    programme.populateRelated('caseStudies', function(err) {
                        
                        if (programme.category === 'none') programme.category = programme.key;
                        locals.data.programme = programme;
                        locals.meta.og.title = locals.meta.twitter.title = programme.title;
                        locals.meta.og.description = locals.meta.twitter.description = programme.overview;
                        locals.meta.og.img = locals.meta.twitter.img = programme.icon.url;
                        locals.meta.og.url = '/programmes/' + programme.key;
                        next(err);
                    });
                }
                else next(err);
                // todo 404
            });

    });

    locals.section = 'programmes';
    view.render('programme');

};
