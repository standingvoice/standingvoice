var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    
    locals.section = 'donate';

    locals.data = {};

    locals.meta = {
      og: {
        type:  'article',
        title: 'Donate',
        url:   '/donate',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      },
      twitter: {
        title: 'Donate',
        description: 'Standing Voice exists to end human rights abuses against marginalised groups.'
      }
    };

    view.on('init', function(next) {
        keystone
            .list('DonationCause')
            .model
            .find()
            .sort('sortOrder')
            .exec(function(err, results) {  
                locals.data.donationCauses = results;
                next(err);
            });
    });
    
    view.on('init', function(next) {
        keystone
            .list('DonationsContent')
            .model
			.find()
            .exec(function(err, content) {
                locals.data.topSectionHeader = content[0].topSectionHeader;
                locals.data.topSectionContent = content[0].topSectionContent;
                locals.data.topSectionCTA = content[0].topSectionCTA;
                locals.data.topSectionButtonText = content[0].topSectionButtonText;
                locals.data.defaultDonationAmount = content[0].defaultDonationAmount;
                
                next(err);
            });
    });
    
    view.render('donations');
    
};
