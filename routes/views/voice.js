var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.section = 'voices';
    locals.data = {
        nav: {}
    };

    locals.meta = {
      og: {
        type:  'article',
      },
      twitter: {
      }
    };

    view.on('init', function(next) {

        keystone
            .list('StandingVoice')
            .model
            .find()
            .sort('sortOrder')
            .exec(function(err, voices) {
                var nav = {};
                for (var i = 0; i < voices.length; i++) {
                    if (voices[i].key === req.params.voice) {
                        nav.current = voices[i];
                        if (i !== 0 && i !== (voices.length-1)) {
                            nav.previous = voices[i-1];
                            nav.next = voices[i+1];
                        }
                        else if (i == 0) {
                            nav.previous = voices[voices.length-1];
                            nav.next = voices[i+1];
                        }
                        else {
                            nav.previous = voices[i-1];
                            nav.next = voices[0];
                        }
                    }
                }
                locals.data.nav = nav;
                next(err);
            });

    });

    view.on('init', function(next) {
        keystone
            .list('StandingVoice')
            .model
            .findOne({
                key: req.params.voice
            })
            .exec(function(err, voice) {
                locals.data.standingVoice = voice;
                if (voice) {
                  locals.meta.og.title = locals.meta.twitter.title = voice.name;
                  locals.meta.og.description = locals.meta.twitter.description = voice.content.introduction;
                  locals.meta.og.img = locals.meta.twitter.img = voice.previewImage.url;
                  locals.meta.og.url = '/standing-voices/' + voice.key;
                }
                next(err);
            });

    });

    view.render('voice');
    
};
