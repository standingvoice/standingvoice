var _ = require('underscore');
var keystone = require('keystone');


exports.initProgrammes = function(req, res, next) {
    
    var locals = res.locals;
    
    locals.user = req.user;
    
    var nav = {
        programmes: { 
            health: [], community: [], advocacy: [], none: []
        }
    };

    keystone
        .list('Programme')
        .model
        .find()
        .exec(function(err, results) {
            
            for (var i = 0; i < results.length; i++) {

                var cat = results[i].category;
                if (Object.keys(nav.programmes).indexOf(cat) > -1) {
                    nav.programmes[cat].push({
                        title: results[i].title,
                        key: results[i].key 
                    });
                }
            
            }

            locals.nav = nav;
            next(err);
        });
    
};

exports.initFooterDate = function(req, res, next) {
	var locals = res.locals;
	
	locals.now = new Date();
	
	next();
};

exports.initOfficeAddresses = function(req, res, next) {
	var locals = res.locals;
	
	keystone
		.list('OfficeAddress')
		.model
		.find()
		.exec(function(err, addresses){
			locals.addresses = addresses;
			next(err);
		});
};


/**
    Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {
    
    if (!req.user) {
        req.flash('error', 'Please sign in to access this page.');
        res.redirect('/keystone/signin');
    } else {
        next();
    }
    
};

exports.rewrites = {
  ssl: function(req, res, next) {
    if (!req.secure) {
      res.redirect('https://www.standingvoice.org' + req.originalUrl);
    }
    else { next(); }
  }
	/*
  nossl: function(req, res, next) {
    if (req.secure) {
      res.redirect('http://www.standingvoice.org' + req.originalUrl);
    }
    else { next(); }
  }
  */
};
