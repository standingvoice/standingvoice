# Standing Voice

### Setup
    cp .env.template .env

Open `.env` and fill out the environment variables. Make sure you are using Node v12.16.0

    npm install
    npm run start

# Notes

### Environments

This project uses a dev and prod environment. However, there is only one data source that feeds both environments. If you make a change to any data models or content via the CMS, it will show up in both environments.

Therefore, the dev environment should be used to test any technical changes that do not have to do with data via the CMS (testing the Stripe integration, upgrading packages, etc).

### SSL

Historically, the prod environment currently only supported HTTP on all pages except the donation page due to issues integrating SSL and KeystoneJS hosted on Heroku. Currently all pages run on https.

However, if in future there are issues then the donations pages can be redirected to:
'https://www.standingvoice-prod.herokuapp.com' 

in middleware.js, and all other pages can go to nossl in index.js, with that appropriate code uncommented in middleware.

### Stripe

`monthly-donation` plan, set to £0.01 / month

### Adding parallax headers

{%- block headerclass %} headertype__parallax{% endblock -%}

{%- block parallaxheaderclass %} id="jsHeaderParallaxHook"{% endblock -%}

{%- block sectionparallaxpartial %}{% include "../partials/headerparallax.swig" with { url: "../img/parallax_placeholder4.jpg" } only %}{% endblock -%}
